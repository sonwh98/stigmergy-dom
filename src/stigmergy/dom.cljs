(ns stigmergy.dom)

(defn get-viewport-dimension []
  [(.-innerWidth js/window) (.-innerHeight js/window)])

(defn select-ancestor [node predicate?]
  (when node
    (let [parent-node (.-parentNode node)]
      (if (predicate? parent-node)
        parent-node
        (select-ancestor parent-node predicate?)))))

(defn iOS? []
  (let [devices #{"iPad" "iPhone" "iPod"}]
    (contains? devices js/navigator.platform)))
